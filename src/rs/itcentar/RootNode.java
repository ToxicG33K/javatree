package rs.itcentar;

public class RootNode extends Node {

    public RootNode(String name) {
        super(name);
        this.setLeaf(false);
    }

}

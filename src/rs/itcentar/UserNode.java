package rs.itcentar;

public class UserNode extends Node {

    private User user;

    public UserNode(User user) {
        super(user.getUsername());
        setLeaf(true);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}

package rs.itcentar;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class UsersByRoleTreeModel implements TreeModel {

    private Node rootNode = new Node("Users By Role");
    private List<RoleNode> roles = Arrays.asList(new RoleNode[]{
        new RoleNode("ADMINS"), new RoleNode("REGISTERED"), new RoleNode("GUESTS")});
    private Map<String, List<UserNode>> users = new HashMap<>();

    public UsersByRoleTreeModel() {
        List<UserNode> adminUsers = Arrays.asList(new UserNode[]{
            new UserNode(new User("user1", "user1@domain.com")),
            new UserNode(new User("user2", "user2@domain.com")),
            new UserNode(new User("user3", "user3@domain.com"))
        });
        users.put("ADMINS", adminUsers);

        List<UserNode> registeredUsers = Arrays.asList(new UserNode[]{
            new UserNode(new User("user4", "user4@domain.com")),
            new UserNode(new User("user5", "user5@domain.com")),
            new UserNode(new User("user6", "user6@domain.com"))
        });
        users.put("REGISTERED", registeredUsers);

        List<UserNode> guestUsers = Arrays.asList(new UserNode[]{
            new UserNode(new User("user7", "user7@domain.com")),
            new UserNode(new User("user8", "user8@domain.com")),
            new UserNode(new User("user9", "user9@domain.com"))
        });
        users.put("GUESTS", guestUsers);
    }

    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (parent instanceof RoleNode) {
            RoleNode n = (RoleNode) parent;
            List<UserNode> list = users.get(n.getName());
            return list.get(index);
        } else {
            return roles.get(index);
        }
    }

    @Override
    public int getChildCount(Object parent) {
        if (parent instanceof Node) {
            return roles.size();
        } else {
            RoleNode n = (RoleNode) parent;
            return users.get(n.getName()).size();
        }
    }

    @Override
    public boolean isLeaf(Object node) {
        Node rnode = (Node) node;
        return rnode.isLeaf();
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
    }

}

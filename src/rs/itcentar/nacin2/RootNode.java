package rs.itcentar.nacin2;

import java.util.Arrays;
import java.util.List;

public class RootNode extends Node {
    
    private List<RoleNode> roles = Arrays.asList(new RoleNode[]{
        new RoleNode("ADMINS"), new RoleNode("REGISTERED"), new RoleNode("GUESTS")});
    
    public RootNode(String name) {
        super(name);
        
        setChildren(roles);
    }
    
}

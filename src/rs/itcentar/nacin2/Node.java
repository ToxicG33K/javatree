package rs.itcentar.nacin2;

import java.util.ArrayList;
import java.util.List;

public abstract class Node {
    private String name;
    private List<? extends Node> children = new ArrayList<>();

    public Node(String name) {
        this.name = name;
    }
    
    public boolean isLeaf() {
        return children.isEmpty();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public final List<? extends Node> getChildren() {
        return children;
    }

    public final void setChildren(List<? extends Node> children) {
        this.children = children;
    }
    
    public int getChildCount() {
        return this.children.size();
    }
    
    public Node getChildByIndex(int index) {
        return this.children.get(index);
    }

    @Override
    public String toString() {
        return name;
    }
}

package rs.itcentar.nacin2;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class UsersByRoleTreeCellRenderer extends DefaultTreeCellRenderer {

    private final ImageIcon rootIcon = new ImageIcon(getClass().getResource("/rs/itcentar/icons/root.png"));
    private final ImageIcon groupIcon = new ImageIcon(getClass().getResource("/rs/itcentar/icons/group.png"));
    private final ImageIcon userIcon = new ImageIcon(getClass().getResource("/rs/itcentar/icons/user.png"));
    private final ImageIcon fieldIcon = new ImageIcon(getClass().getResource("/rs/itcentar/icons/field.png"));
    
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
            boolean expanded, boolean leaf, int row, boolean hasFocus) {
        
        if(value instanceof RootNode) {
            setIcon(rootIcon);
            setLeafIcon(rootIcon);
            setOpenIcon(rootIcon);
            setClosedIcon(rootIcon);
        } else if(value instanceof RoleNode) {
            setIcon(groupIcon);
            setLeafIcon(groupIcon);
            setOpenIcon(groupIcon);
            setClosedIcon(groupIcon);
        } else if(value instanceof UserNode) {
            setIcon(userIcon);
            setLeafIcon(userIcon);
            setOpenIcon(userIcon);
            setClosedIcon(userIcon);
        } else {// FieldNode
            setIcon(fieldIcon);
            setLeafIcon(fieldIcon);
            setOpenIcon(fieldIcon);
            setClosedIcon(fieldIcon);
        }
        super.getTreeCellRendererComponent(tree, value, sel, expanded,
                leaf, row, hasFocus);
        return this;
    }
    
}

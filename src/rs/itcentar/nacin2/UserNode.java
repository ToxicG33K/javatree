package rs.itcentar.nacin2;

import java.util.Arrays;
import java.util.List;

public class UserNode extends Node {
    
    private User user;
    
    public UserNode(User user) {
        super(user.getUsername());
        this.user = user;
        
        List<FieldNode> fields = Arrays.asList(new FieldNode[] {
            new FieldNode(user.getEmail())
        });
        setChildren(fields);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}

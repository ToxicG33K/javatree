package rs.itcentar.nacin2;

import java.util.Arrays;
import java.util.List;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class UsersByRoleTreeModel implements TreeModel {

    private RootNode rootNode = new RootNode("ROOT");

    private List<UserNode> adminUsers = Arrays.asList(new UserNode[]{
        new UserNode(new User("user1", "user1@domain.com")),
        new UserNode(new User("user2", "user2@domain.com")),
        new UserNode(new User("user3", "user3@domain.com"))
    });
    
    List<UserNode> registeredUsers = Arrays.asList(new UserNode[]{
        new UserNode(new User("user4", "user4@domain.com")),
        new UserNode(new User("user5", "user5@domain.com")),
        new UserNode(new User("user6", "user6@domain.com"))
    });
    
    List<UserNode> guestUsers = Arrays.asList(new UserNode[]{
        new UserNode(new User("user7", "user7@domain.com")),
        new UserNode(new User("user8", "user8@domain.com")),
        new UserNode(new User("user9", "user9@domain.com"))
    });

    public UsersByRoleTreeModel() {
        for(Node n : rootNode.getChildren()) {
            switch(n.getName()) {
                case "ADMINS":
                    n.setChildren(adminUsers);
                    break;
                case "REGISTERED":
                    n.setChildren(registeredUsers);
                    break;
                case "GUESTS":
                    n.setChildren(guestUsers);
                    break;
            }
        }
    }
    
    @Override
    public Object getRoot() {
        return rootNode;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Node node = (Node) parent;
        Node childNode = node.getChildByIndex(index);
        return childNode;
    }

    @Override
    public int getChildCount(Object parent) {
        return ((Node) parent).getChildCount();
    }

    @Override
    public boolean isLeaf(Object node) {
        return ((Node) node).isLeaf();
    }
/**************************************************************************/
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {}

    @Override
    public int getIndexOfChild(Object parent, Object child) { return 0; }

    @Override
    public void addTreeModelListener(TreeModelListener l) {}

    @Override
    public void removeTreeModelListener(TreeModelListener l) {}
    
}
